def number_crossword_clues(grid):
    n = len(grid)
    m = len(grid[0])
    words = 0
    words_array = [[0 for _ in range(m)] for _ in range(n)]
    res_array = []

    def traverse_rows(row):
        if row >= n:
            return
        process_row(row, 0)
        traverse_rows(row + 1)

    def traverse_columns(col):
        if col >= m:
            return
        process_column(0, col)
        traverse_columns(col + 1)

    def process_row(row, col):
        if col >= m:
            return
        if grid[row][col] == " ":
            start_col = col
            col = find_end_of_sequence_in_row(row, col)
            if col - start_col > 2:
                nonlocal words
                words += 1
                words_array[row][start_col] = words
                res_array.append((row, start_col))
        process_row(row, col + 1)

    def process_column(row, col):
        if row >= n:
            return
        if grid[row][col] == " ":
            start_row = row
            row = find_end_of_sequence_in_col(row, col)
            if row - start_row > 2:
                nonlocal words
                words += 1
                words_array[start_row][col] = words
                res_array.append((start_row, col))
        process_column(row + 1, col)

    def find_end_of_sequence_in_row(row, col):
        if col >= m or grid[row][col] == "#":
            return col
        return find_end_of_sequence_in_row(row, col + 1)

    def find_end_of_sequence_in_col(row, col):
        if row >= n or grid[row][col] == "#":
            return row
        return find_end_of_sequence_in_col(row + 1, col)

    
    traverse_rows(0)
    traverse_columns(0)

    
    res_array = sorted(set(res_array))
    res_array = [(x, y, i + 1) for i, (x, y) in enumerate(res_array)]

    return res_array


grid = [
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', '#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#'],
    [' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
    [' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' '],
    [' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' '],
    ['#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
]


result = number_crossword_clues(grid)
for item in result:
    print(item)
