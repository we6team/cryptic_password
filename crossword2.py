WHITE = ' '
BLACK = '#'
SIZE = 15

def matrix_gen(crossword: str):
    return list(zip([(i, j) for i in range(SIZE) for j in range(SIZE)], [ele for ele in crossword if ele in (WHITE, BLACK)]))

def checkdown(matrix, i, j):
    for x in range(i, SIZE):
        if matrix[x * SIZE + j][1] == BLACK:
            break
        if "D" not in matrix[x * SIZE + j][1]:
            matrix[x * SIZE + j] = ((x, j), matrix[x * SIZE + j][1] + "D")

def checkacross(matrix, i, j):
    for y in range(j, SIZE):
        if matrix[i * SIZE + y][1] == BLACK:
            break
        if "A" not in matrix[i * SIZE + y][1]:
            matrix[i * SIZE + y] = ((i, y), matrix[i * SIZE + y][1] + "A")

def origin(matrix):
    for i in range(SIZE):
        for j in range(SIZE):
            pos = i * SIZE + j
            cell = matrix[pos]
            if cell[1] == BLACK:
                continue
            if i == 0 or matrix[(i - 1) * SIZE + j][1] == BLACK:
                checkdown(matrix, i, j)
            if j == 0 or matrix[i * SIZE + (j - 1)][1] == BLACK:
                checkacross(matrix, i, j)
    return matrix

# Input crossword puzzle as a string
inp = '''
     
 # # # ### # # 
     #     
 # # # # # # # 
    ####
 # ### # # # # 
     #     
 # # # # # # # 
     #     
 # # # ### # 
####         
 # # # # # # # 
    #     
 # # ### # # # 
    #     
'''

inp = inp.replace("\n", "")  # Remove newline characters

# Generate the matrix from the input
matrix = matrix_gen(inp)

# Process the matrix
result = origin(matrix)

# Print the result in a readable format
for i in range(SIZE):
    for j in range(SIZE):
        print(result[i * SIZE + j][1], end=' ')
    print()

