WHITE = ' '
BLACK = '#'
SIZE = 15


def matrix_gen(crossword: str):
	return list(zip([(i, j)for i in range(SIZE) for j in range (SIZE)], [ele for ele in crossword]))




inp= '''
        #     
 # # # ### # # 
        #     
 # # # # # # # 
           ####
 # ### # # # # 
     #         
 # # # # # # # 
         #     
 # # # # ### # 
####           
 # # # # # # # 
      #        
 # # ### # # # 
      #        
'''

def format_matrix(m : list):
	m = m.replace("\n","").replace(" ", "W").replace("#", "B")
	return m
m = matrix_gen(format_matrix(inp))

print(m)


