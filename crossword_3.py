def crossword_numbering(crossword):
    size = len(crossword)
    label_grid = [[' ' if c == ' ' else '#' for c in row] for row in crossword]
    numbering = []
    num = 1
    
    for y, row in enumerate(crossword):
        for x, cell in enumerate(row):
            if cell == ' ':
                is_across = (x == 0 or crossword[y][x - 1] == '#')
                is_down = (y == 0 or crossword[y - 1][x] == '#')
                
                if is_across:
                    label_grid[y][x] = str(num)
                    numbering.append((y, x, num))
                
                if is_down:
                    if not is_across:  # Don't number twice if both across and down start here
                        label_grid[y][x] = str(num)
                    numbering.append((y, x, num))
                
                if is_across or is_down:
                    num += 1
    
    return label_grid, numbering

crossword = [
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#'],
[' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ',' ',' ',' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' '],
['#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ',' ',' ',' ']    
]

# Run the crossword numbering function
numbering = crossword_numbering(crossword)



# Print the numbered clues in (row, column, numbering) format
for num in numbering:
    print(num)
