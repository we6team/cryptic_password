import numpy as np
grid = np.array([
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#'],
[' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' '],
['#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#', ' ', '#', ' ', '#', ' '],
[' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
])

n = len(grid) 
m = len(grid[0]) 
words = 0
words_array = [[0 for _ in range(m)] for _ in range(n)]
res_array = []
for i in range(n):
    char=0
    charArray = []
    for j in range(m):
        if grid[i][j]==" ":
            char+=1
            charArray.append(j)
        if grid[i][j] == "#" or j==m-1:
            if char>2:
                words+=1
                words_array[i][charArray[0]]=words
                temp_res = (i,charArray[0])
                res_array.append(temp_res)
            char=0
            charArray= []

for j in range(m):
    char=0
    charArray = []
    for i in range(n):    
        if grid[i][j]==" ":
            char+=1
            charArray.append(i)
        if grid[i][j] == "#" or i==n-1:
            if char>2:
                words+=1
                words_array[charArray[0]][j]=words
                temp_res = (charArray[0],j)
                res_array.append(temp_res)
            char=0
res_array = sorted(set(res_array))
res_array = [(x, y, i + 1) for i, (x, y) in enumerate(res_array)]
print(res_array)
